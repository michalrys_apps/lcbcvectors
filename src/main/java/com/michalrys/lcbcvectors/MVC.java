package com.michalrys.lcbcvectors;

public interface MVC {
    interface View {
        void runWithInputArgs();
    }

    interface Controller {
        void runWithInputArgs(String[] inputArgs);

        void runWithDragAndDropFile(String[] inputArgs);
    }
}
