package com.michalrys.lcbcvectors;

import com.michalrys.lcbcvectors.view.cmd.CMDView;

import java.time.LocalDate;

public class RunApp {
    public static void main(String[] args) {
        if (isLicenseValid()) {
            CMDView.main(args);
        } else {
            System.out.println("Please ask MR.");
        }
    }

    private static boolean isLicenseValid() {
        LocalDate licenseDate = LocalDate.parse("2019-12-01");
        if (LocalDate.now().isAfter(licenseDate)) {
            return false;
        }
        return true;
    }
}
