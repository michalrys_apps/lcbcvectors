package com.michalrys.lcbcvectors;

import com.michalrys.lcbcvectors.model.settings.Settings;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.InputStream;

public class RunAppJavaFX extends Application {
    public static String[] inputArgs;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Settings.run();

        // For maven project fxml file must be in resources! Then it is copied to TARGET directory.
        Parent root = FXMLLoader.load(getClass().getResource("/view/Preview.fxml"));

        // For maven project png files are not copied to TARGET... so I changed extension to fxml :P
        InputStream resourceAsStream = getClass().getResourceAsStream("/view/icon.fxml");
        primaryStage.getIcons().add(new Image(resourceAsStream));
        resourceAsStream.close();

        primaryStage.setScene(new Scene(root));
        primaryStage.setTitle("LC/BC Vectors <--");
        primaryStage.setAlwaysOnTop(true);
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    public static void main(String[] args) {
        inputArgs = args;
        launch(args);
    }
}