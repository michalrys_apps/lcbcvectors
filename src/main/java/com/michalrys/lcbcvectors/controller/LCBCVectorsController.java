package com.michalrys.lcbcvectors.controller;

import com.michalrys.lcbcvectors.MVC;
import com.michalrys.lcbcvectors.model.lcbcvectors.LCBCVectors;
import com.michalrys.lcbcvectors.model.lcbcvectors.LCBCVectorsImpl;
import com.michalrys.lcbcvectors.model.settings.Settings;

import java.io.File;

public class LCBCVectorsController implements MVC.Controller {
    private MVC.View view;

    public LCBCVectorsController(MVC.View view) {
        this.view = view;
    }

    @Override
    public void runWithInputArgs(String[] inputArgs) {
        if (isInputArgsEmpty(inputArgs)) {
            return;
        }
        File file = extractFirstFile(inputArgs);
        if(doesNotExist(file)) {
            return;
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                Settings.status.setRunning(true);
                Settings.status.appendNewLine("Extracting for given file: " + file.getName());
                LCBCVectors lcbcVectors = new LCBCVectorsImpl();
                lcbcVectors.setExcelFile(file);
                lcbcVectors.setUp();
                lcbcVectors.extractData();
                lcbcVectors.writeToTCLFile();
                lcbcVectors.getTclFilePath();
                Settings.status.setRunning(false);
            }
        }).start();
    }

    @Override
    public void runWithDragAndDropFile(String[] inputArgs) {
        File file = extractFirstFile(inputArgs);
        new Thread(new Runnable() {
            @Override
            public void run() {
                Settings.status.setRunning(true);
                Settings.status.appendNewLine("" +
                        "<-- <-- <-- <-- <-- <-- <-- <-- <-- <-- <-- <-- <-- <-- <-- <-- <-- <-- <-- <-- <--");
                Settings.status.appendNewLine("Extracting for dropped file: " + file.getName());
                LCBCVectors lcbcVectors = new LCBCVectorsImpl();
                lcbcVectors.setExcelFile(file);
                lcbcVectors.setUp();
                lcbcVectors.extractData();
                lcbcVectors.writeToTCLFile();
            }
        }).start();
        Settings.status.setRunning(false);
    }

    private boolean isInputArgsEmpty(String[] args) {
        if (args.length == 0) {
            Settings.status.appendNewLine("Application was run without input args. Nothing was done.");
            return true;
        }
        return false;
    }

    private File extractFirstFile(String[] args) {
        String arg = args[0];
        File file = new File(arg);
        return file;
    }

    private boolean doesNotExist(File file) {
        if (!file.exists()) {
            Settings.status.appendNewLine("Wrong given file: " + file.getAbsolutePath() + ". Nothing was done.");
            return true;
        }
        return false;
    }
}
