package com.michalrys.lcbcvectors.model.excel;

import com.michalrys.lcbcvectors.model.lccollector.LCCollector;
import com.michalrys.lcbcvectors.model.loadvector.VectorType;

import java.io.File;
import java.util.List;
import java.util.Map;

public interface ExcelFile {
    boolean readFile(File file);

    boolean isExcelFileLCBCParsedValid();

    Map<String, LCCollector> extractLoadData(List<VectorType> listOfVectorTypeToExtract, SumAlgorithm sumAlgorithm);
}
