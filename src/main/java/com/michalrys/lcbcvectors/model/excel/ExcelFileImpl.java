package com.michalrys.lcbcvectors.model.excel;

import com.michalrys.lcbcvectors.model.lccollector.LCCollecorFactoryImpl;
import com.michalrys.lcbcvectors.model.lccollector.LCCollector;
import com.michalrys.lcbcvectors.model.lccollector.LCCollectorFactory;
import com.michalrys.lcbcvectors.model.loadvector.LoadVector;
import com.michalrys.lcbcvectors.model.loadvector.LoadVectorFactory;
import com.michalrys.lcbcvectors.model.loadvector.LoadVectorFactoryImpl;
import com.michalrys.lcbcvectors.model.loadvector.VectorType;
import com.michalrys.lcbcvectors.model.settings.Settings;
import com.michalrys.lcbcvectors.model.status.StatusCMDPrinter;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ExcelFileImpl implements ExcelFile {
    private static final String FORCES = "Forces";
    private static final int FIRST_ROW = 12;
    private static final int CELL_ID_LOADCASE_NAME = 5;
    private static final int CELL_ID_FIRST = 6;
    private static final int ROW_ID_LOAD_TYPE = 10;
    private static final int ROW_ID_LOAD_ID = 9;
    private static final int ROW_ID_CID = 8;
    private static final int ROW_ID_FACTOR = 6;
    private static final int ROW_ID_X_COORDINATE = 5;
    private static final int ROW_ID_Y_COORDINATE = 4;
    private static final int ROW_ID_Z_COORDINATE = 3;
    private static final int ROW_ID_NODE = 1;
    Workbook workbook;
    Sheet sheet;
    Row row;
    int rowId = FIRST_ROW;
    Cell cell;
    int cellId;
    String loadCaseName;
    Map<String, LCCollector> lcCollectors = new LinkedHashMap<>();
    LCCollectorFactory lcCollectorFactory = new LCCollecorFactoryImpl();
    LoadVectorFactory loadVectorFactory = new LoadVectorFactoryImpl();

    @Override
    public boolean readFile(File file) {
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(file);
            workbook = new XSSFWorkbook(fileInputStream);
            sheet = workbook.getSheet(FORCES);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public boolean isExcelFileLCBCParsedValid() {
        row = sheet.getRow(11);
        cell = row.getCell(1);
        if (!cell.getStringCellValue().equals("SPC")) {
            return false;
        }
        cell = row.getCell(2);
        if (!cell.getStringCellValue().equals("GLF")) {
            return false;
        }
        cell = row.getCell(3);
        if (!cell.getStringCellValue().equals("LOAD")) {
            return false;
        }
        row = sheet.getRow(1);
        cell = row.getCell(5);
        if (!cell.getStringCellValue().equals("node: ")) {
            return false;
        }
        return true;
    }

    @Override
    public Map<String, LCCollector> extractLoadData(
            List<VectorType> listOfVectorTypeToExtract, SumAlgorithm sumAlgorithm) {
        while ((row = sheet.getRow(rowId++)) != null) {
            cell = row.getCell(CELL_ID_LOADCASE_NAME);
            loadCaseName = cell.getStringCellValue();
            StatusCMDPrinter.print("Load case: " + loadCaseName);
            Settings.status.appendNewLine("Load case: " + loadCaseName);

            cellId = CELL_ID_FIRST - 1;
            while ((cell = row.getCell(++cellId)) != null) {
                double loadFactor = cell.getNumericCellValue();

                if (loadFactor != 0) {
                    // 1 - read load type
                    Row rowLoadType = sheet.getRow(ROW_ID_LOAD_TYPE);
                    Cell cellLoadType = rowLoadType.getCell(cellId);
                    String loadType = cellLoadType.getStringCellValue();

                    if (isNotValidVectorType(loadType, listOfVectorTypeToExtract)) {
                        continue;
                    }

                    // 2.1 - set LOAD VECTOR -> load id
                    LoadVector loadVector = loadVectorFactory.get(VectorType.valueOf(loadType));
                    Row rowLoadId = sheet.getRow(ROW_ID_LOAD_ID);
                    Cell cellLoadId = rowLoadId.getCell(cellId);
                    loadVector.setId(cellLoadId.getNumericCellValue());

                    // 2.2. - set LOAD VECTOR -> CID
                    Row rowCID = sheet.getRow(ROW_ID_CID);
                    Cell cellCID = rowCID.getCell(cellId);
                    loadVector.setCID(cellCID.getStringCellValue());

                    // 2.3. - set LOAD VECTOR -> factor
                    Row rowFactor = sheet.getRow(ROW_ID_FACTOR);
                    Cell cellFactor = rowFactor.getCell(cellId);
                    if (loadType.equals(VectorType.GRAV.toString())) { //TODO fix it in LCBC extractor for grav
                        loadVector.setFactor(String.valueOf(cellFactor.getNumericCellValue()));
                    } else {
                        loadVector.setFactor(cellFactor.getStringCellValue());
                    }
                    loadVector.multiplyFactorByLoadFactor(loadFactor);

                    // 2.4 - set X,Y,Z,NODE:
                    Row rowXCoordinate = sheet.getRow(ROW_ID_X_COORDINATE);
                    Cell cellXCoordinate = rowXCoordinate.getCell(cellId);
                    String x = "";
                    if (loadType.equals(VectorType.GRAV.toString())) {
                        x = String.valueOf(cellXCoordinate.getNumericCellValue());
                    } else {
                        x = cellXCoordinate.getStringCellValue();
                    }
                    Row rowYCoordinate = sheet.getRow(ROW_ID_Y_COORDINATE);
                    Cell cellYCoordinate = rowYCoordinate.getCell(cellId);
                    String y = "";
                    if (loadType.equals(VectorType.GRAV.toString())) {
                        y = String.valueOf(cellYCoordinate.getNumericCellValue());
                    } else {
                        y = cellYCoordinate.getStringCellValue();
                    }
                    Row rowZCoordinate = sheet.getRow(ROW_ID_Z_COORDINATE);
                    Cell cellZCoordinate = rowZCoordinate.getCell(cellId);
                    String z = "";
                    if (loadType.equals(VectorType.GRAV.toString())) {
                        z = String.valueOf(cellZCoordinate.getNumericCellValue());
                    } else {
                        z = cellZCoordinate.getStringCellValue();
                    }
                    Row rowNode = sheet.getRow(ROW_ID_NODE);
                    Cell cellNode = rowNode.getCell(cellId);
                    String node = cellNode.getStringCellValue();

                    List<LoadVector> currentLoadVectors = loadVector.setXYZNodeAndCalculateAnglesMagnitude(x, y, z, node);

                    //FIXME delete it later
                    for (LoadVector currentLoadVector : currentLoadVectors) {
                        StatusCMDPrinter.print("  -> " + currentLoadVector.toString());
                        Settings.status.appendNewLine("  -> " + currentLoadVector.toString());
                    }

                    // 3 - put LOAD VECTOR to lcCollector - if it does not exist, it will be created
                    String lcColName = loadCaseName + "_" + loadType;
                    StatusCMDPrinter.print("     -> " + lcColName);
                    Settings.status.appendNewLine("     -> " + lcColName);
                    LCCollector lcCollector;
                    if (lcCollectors.containsKey(lcColName)) {
                        lcCollector = lcCollectors.get(lcColName);
                    } else {
                        lcCollector = lcCollectorFactory.get(VectorType.valueOf(loadType));
                        lcCollector.setName(lcColName);
                    }
                    lcCollector.addLoadVectors(currentLoadVectors);
                    lcCollectors.put(lcColName, lcCollector);
                }
            }
        }
        StatusCMDPrinter.print("Extraction is done.");
        Settings.status.appendNewLine("Extraction is done.");
        return lcCollectors;
    }

    private boolean isNotValidVectorType(String loadType, List<VectorType> listOfVectorTypeToExtract) {
        //List<VectorType> vectorTypes = Arrays.asList(VectorType.values());
        for (VectorType vectorType : listOfVectorTypeToExtract) {
            if (loadType.equals(vectorType.toString()))
                return false;
        }
        return true;
    }
}
