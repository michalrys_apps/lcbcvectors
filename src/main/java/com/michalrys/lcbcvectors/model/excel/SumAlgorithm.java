package com.michalrys.lcbcvectors.model.excel;

public enum SumAlgorithm {
    BY_LOCATION("Sum vectors with the same location - so, we have total vector at point."),
    BY_LOCATION_AND_BY_DIRECTION ("Sum vectors with the same location and direction ");

    private String description;

    SumAlgorithm(String description) {
        this.description = description;
    }

    public static SumAlgorithm setDefault() {
        return SumAlgorithm.BY_LOCATION_AND_BY_DIRECTION;
    }
}
