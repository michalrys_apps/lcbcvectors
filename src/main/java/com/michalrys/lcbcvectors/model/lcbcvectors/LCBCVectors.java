package com.michalrys.lcbcvectors.model.lcbcvectors;

import java.io.File;

public interface LCBCVectors {
    boolean setExcelFile(File file);

    boolean setUp();

    boolean extractData();

    boolean writeToTCLFile();

    String getTclFilePath();
}
