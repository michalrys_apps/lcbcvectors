package com.michalrys.lcbcvectors.model.lcbcvectors;

import com.michalrys.lcbcvectors.model.excel.ExcelFile;
import com.michalrys.lcbcvectors.model.excel.ExcelFileImpl;
import com.michalrys.lcbcvectors.model.excel.SumAlgorithm;
import com.michalrys.lcbcvectors.model.lccollector.LCCollector;
import com.michalrys.lcbcvectors.model.loadvector.VectorType;
import com.michalrys.lcbcvectors.model.settings.Settings;
import com.michalrys.lcbcvectors.model.status.StatusCMDPrinter;
import com.michalrys.lcbcvectors.model.tclmacro.TCLMacro;
import com.michalrys.lcbcvectors.model.tclmacro.TCLMacroImpl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class LCBCVectorsImpl implements LCBCVectors {
    private File tclFile;
    private ExcelFile excelFile = new ExcelFileImpl();
    private Map<String, LCCollector> lcCollectors = new LinkedHashMap();
    private TCLMacro tclMacro = new TCLMacroImpl();
    private List<VectorType> listOfVectorTypeToExtract;
    private SumAlgorithm sumAlgorithm;

    @Override
    public boolean setExcelFile(File file) {
        String[] split = file.getName().split("\\.");
        if (split.length > 2) {
            System.out.println("\tWrong name - it is not allowed to have \\. in file name.");
            Settings.status.appendNewLine("\tWrong name - it is not allowed to have \\. in file name.");
            return false;
        }
        if (!split[1].equals("xlsx") && !split[1].equals("XLSX")) {
            System.out.println("\t Wrong extension - it must be xlsx.");
            Settings.status.appendNewLine("\t Wrong extension - it must be xlsx.");
            return false;
        }

        excelFile.readFile(file);
        boolean fileValid = excelFile.isExcelFileLCBCParsedValid();

        setTclFile(file);
        return fileValid;
    }

    private void setTclFile(File file) {
        String path = file.getAbsolutePath();
        String name = file.getName();
        String nameOnly = name.split("\\.")[0];

        String tclPath = path.replaceAll(name, "");
        tclPath = tclPath + nameOnly + "_LCBCVectors_macro_"
                + LocalDateTime.now().format(DateTimeFormatter.ofPattern("YYMMdd-HHmmss")) + ".tcl";

        tclFile = new File(tclPath);
    }

    @Override
    public boolean setUp() {
        listOfVectorTypeToExtract = Settings.viewOptions.getVectorTypes();
        return true;
    }

    @Override
    public boolean extractData() {
        lcCollectors = excelFile.extractLoadData(listOfVectorTypeToExtract, Settings.sumAlgorithm);
        return true;
    }

    @Override
    public boolean writeToTCLFile() {
        StatusCMDPrinter.print("Creating TCL macro file...");
        Settings.status.appendNewLine("Creating TCL macro file...");
        tclMacro.setLCCollectors(lcCollectors);
        String macro = tclMacro.prepareMacro();

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(tclFile))) {
            writer.write(macro);
        } catch (IOException e) {
            e.printStackTrace();
        }
        StatusCMDPrinter.print("TCL macro has been written in: ");
        StatusCMDPrinter.print("  -> " + tclFile.getName());
        Settings.status.appendNewLine("TCL macro has been written in: ");
        Settings.status.appendNewLine("  -> " + tclFile.getName());
        Settings.status.appendNewLine("" +
                "<-- <-- <-- <-- <-- <-- <-- <-- <-- <-- <-- <-- <-- <-- <-- <-- <-- <-- <-- <-- <--");
        return true;
    }

    @Override
    public String getTclFilePath() {
        return tclFile.getAbsolutePath();
    }
}