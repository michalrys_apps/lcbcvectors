package com.michalrys.lcbcvectors.model.lccollector;

import com.michalrys.lcbcvectors.model.loadvector.VectorType;

public class LCCollecorFactoryImpl implements LCCollectorFactory {
    @Override
    public LCCollector get(VectorType type) {
        switch (type) {
            case FORCE:
                return new LCForces();
            case MOMENT:
                return new LCMoments();
            case PLOAD4:
                return new LCPressures();
            case GRAV:
                return new LCGravities();
        }
        return null;
    }
}
