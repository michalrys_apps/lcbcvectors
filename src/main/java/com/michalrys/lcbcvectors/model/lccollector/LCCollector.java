package com.michalrys.lcbcvectors.model.lccollector;

import com.michalrys.lcbcvectors.model.loadvector.LoadVector;
import com.michalrys.lcbcvectors.model.loadvector.VectorType;

import java.util.List;

public interface LCCollector {
    VectorType getType();

    void setName(String name);

    String getName();

    void addLoadVectors(List<LoadVector> currentLoadVectors);

    List<LoadVector> getLoadVectors();

}
