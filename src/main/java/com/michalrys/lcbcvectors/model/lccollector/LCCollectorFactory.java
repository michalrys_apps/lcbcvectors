package com.michalrys.lcbcvectors.model.lccollector;

import com.michalrys.lcbcvectors.model.loadvector.VectorType;

public interface LCCollectorFactory {
    LCCollector get(VectorType type);
}
