package com.michalrys.lcbcvectors.model.lccollector;

import com.michalrys.lcbcvectors.model.excel.SumAlgorithm;
import com.michalrys.lcbcvectors.model.loadvector.LoadVector;
import com.michalrys.lcbcvectors.model.loadvector.VectorType;
import com.michalrys.lcbcvectors.model.settings.Settings;

import java.util.ArrayList;
import java.util.List;

public class LCPressures implements LCCollector {
    private VectorType type;
    private String name;
    private List<LoadVector> loads = new ArrayList<>();

    public LCPressures() {
        this.type = VectorType.PLOAD4;
    }

    @Override
    public VectorType getType() {
        return type;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void addLoadVectors(List<LoadVector> loadVectorsToAdd) {
        //check if there are the same - if yes - add magnitudes
        for (LoadVector loadVectorToAdd : loadVectorsToAdd) {
            // case 1: sum vectors with the same location and direction
            if (loads.contains(loadVectorToAdd)) {
                int i = loads.indexOf(loadVectorToAdd);
                LoadVector loadVectorExisting = loads.get(i);
                double magnitudeToAdd = Double.parseDouble(loadVectorToAdd.getMagnitude());
                loadVectorExisting.increaseMagnitude(magnitudeToAdd);
                loads.remove(i);
                loads.add(i, loadVectorExisting);
                continue;
            }
            //case 2: sum vectors with the same location only - new direction will be calculated.
            boolean vectorShouldBeAdd = true;
            if(Settings.sumAlgorithm.equals(SumAlgorithm.BY_LOCATION)) {
                for(LoadVector load : loads) {
                    String loadNode = load.getNode();
                    String loadToAddNode = loadVectorToAdd.getNode();

                    if(loadNode.equals(loadToAddNode)) {
                        load.addVector(loadVectorToAdd);
                        vectorShouldBeAdd = false;
                    }
                }
            }
            if(vectorShouldBeAdd) {
                loads.add(loadVectorToAdd);
            }
        }
    }

    @Override
    public List<LoadVector> getLoadVectors() {
        return loads;
    }
}