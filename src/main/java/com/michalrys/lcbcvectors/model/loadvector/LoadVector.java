package com.michalrys.lcbcvectors.model.loadvector;

import java.util.List;

public interface LoadVector {
    VectorType getLoadType();

    void setId(double numericCellValue);

    String getId();

    void setCID(String numericCellValue);

    String getCID();

    void setFactor(String factor);

    void multiplyFactorByLoadFactor(double loadFactor);

    String getFactor();

    void setX(String xCoordinate);

    String getX();

    void setY(String yCoordinate);

    String getY();

    void setZ(String zCoordinate);

    String getZ();

    void setNode(String node);

    String getNode();

//    void calculateAngleXY();
//
//    void calculateAngleYZ();
//
//    double getAngleXY();
//
//    double getAngleYZ();

    void calculateCosAlfaXYZAndMagnitude();

    double getCosAlfaX();

    double getCosAlfaY();

    double getCosAlfaZ();

    String getMagnitude();

    List<LoadVector> setXYZNodeAndCalculateAnglesMagnitude(String x, String y, String z, String node);

    void increaseMagnitude(double magnitudeToAdd);

    void addVector(LoadVector loadVectorToAdd);
}
