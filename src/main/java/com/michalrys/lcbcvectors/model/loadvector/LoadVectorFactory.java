package com.michalrys.lcbcvectors.model.loadvector;

public interface LoadVectorFactory {
    LoadVector get(VectorType type);
}
