package com.michalrys.lcbcvectors.model.loadvector;

public class LoadVectorFactoryImpl implements LoadVectorFactory {
    @Override
    public LoadVector get(VectorType type) {
        switch (type) {
            case FORCE:
                return new ForceVector();
            case MOMENT:
                return new MomentVector();
            case PLOAD4:
                return new PressureVector();
            case GRAV:
                return new GravityVector();
        }
        return null;
    }
}
