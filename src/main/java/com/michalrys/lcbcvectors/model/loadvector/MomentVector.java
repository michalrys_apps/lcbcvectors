package com.michalrys.lcbcvectors.model.loadvector;

import com.michalrys.lcbcvectors.model.settings.Settings;
import com.michalrys.lcbcvectors.model.status.StatusCMDPrinter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MomentVector implements LoadVector {
    private VectorType loadType;
    private String id;
    private String cid;
    private String node;
    private double x;
    private double y;
    private double z;
    private double factor;
    private double cosAlfaX;
    private double cosAlfaY;
    private double cosAlfaZ;
    private double magnitude;

    public MomentVector() {
        this.loadType = VectorType.MOMENT;
    }

    @Override
    public VectorType getLoadType() {
        return loadType;
    }

    @Override
    public void setId(double id) {
        this.id = String.valueOf(id).replaceAll("\\..*", "");
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setCID(String cid) {
        this.cid = String.valueOf(cid).replaceAll("\\..*", "");
    }

    @Override
    public String getCID() {
        return cid;
    }

    @Override
    public void setFactor(String factor) {
        this.factor = Double.valueOf(factor);
    }

    @Override
    public void multiplyFactorByLoadFactor(double loadFactor) {
        factor *= loadFactor;
    }

    @Override
    public String getFactor() {
        return String.valueOf(factor);
    }

    @Override
    public void setX(String xCoordinate) {
        this.x = Double.parseDouble(xCoordinate);
    }

    @Override
    public String getX() {
        return String.valueOf(x);
    }

    @Override
    public void setY(String yCoordinate) {
        this.y = Double.parseDouble(yCoordinate);
    }

    @Override
    public String getY() {
        return String.valueOf(y);
    }

    @Override
    public void setZ(String zCoordinate) {
        this.z = Double.parseDouble(zCoordinate);
    }

    @Override
    public String getZ() {
        return String.valueOf(z);
    }

    @Override
    public void setNode(String node) {
        this.node = node;
    }

    @Override
    public String getNode() {
        return node;
    }

    @Override
    public void calculateCosAlfaXYZAndMagnitude() {
        double magnitudeFromComponents = Math.sqrt(x * x + y * y + z * z);
        cosAlfaX = x / magnitudeFromComponents;
        cosAlfaX = Math.round(cosAlfaX * 10000.0) / 10000.0;
        cosAlfaY = y / magnitudeFromComponents;
        cosAlfaY = Math.round(cosAlfaY * 10000.0) / 10000.0;
        cosAlfaZ = z / magnitudeFromComponents;
        cosAlfaZ = Math.round(cosAlfaZ * 10000.0) / 10000.0;

        magnitude = magnitudeFromComponents * factor;

        x = magnitude * cosAlfaX;
        x = Math.round(x * 10000.0) / 10000.0;
        y = magnitude * cosAlfaY;
        y = Math.round(y * 10000.0) / 10000.0;
        z = magnitude * cosAlfaZ;
        z = Math.round(z * 10000.0) / 10000.0;
    }

    @Override
    public double getCosAlfaX() {
        return cosAlfaX;
    }

    @Override
    public double getCosAlfaY() {
        return cosAlfaY;
    }

    @Override
    public double getCosAlfaZ() {
        return cosAlfaZ;
    }

    @Override
    public String getMagnitude() {
        return String.valueOf(magnitude);
    }

    @Override
    public List<LoadVector> setXYZNodeAndCalculateAnglesMagnitude(String x, String y, String z, String node) {
        List<String> splitNode = new ArrayList<>();
        List<String> splitX = new ArrayList<>();
        List<String> splitY = new ArrayList<>();
        List<String> splitZ = new ArrayList<>();

        splitNode.addAll(Arrays.asList(node.split(",")));
        splitX.addAll(Arrays.asList(x.split(",")));
        splitY.addAll(Arrays.asList(y.split(",")));
        splitZ.addAll(Arrays.asList(z.split(",")));

        int howManyNodes = splitNode.size();
        duplicateElementsInListGivenTimes(splitX, howManyNodes);
        duplicateElementsInListGivenTimes(splitY, howManyNodes);
        duplicateElementsInListGivenTimes(splitZ, howManyNodes);

        List<LoadVector> results = new ArrayList<>();
        for (int i = 0; i < splitNode.size(); i++) {
            LoadVector loadVectorDuplicated = duplicateCurrentLoadVector();

            loadVectorDuplicated.setNode(splitNode.get(i));
            loadVectorDuplicated.setX(splitX.get(i));
            loadVectorDuplicated.setY(splitY.get(i));
            loadVectorDuplicated.setZ(splitZ.get(i));
            loadVectorDuplicated.calculateCosAlfaXYZAndMagnitude();

            results.add(loadVectorDuplicated);
        }
        return results;
    }

    private LoadVector duplicateCurrentLoadVector() {
        LoadVector loadVectorDuplicate = new LoadVectorFactoryImpl().get(this.getLoadType());
        loadVectorDuplicate.setCID(this.getCID());
        loadVectorDuplicate.setFactor(this.getFactor());
        loadVectorDuplicate.setId(Integer.parseInt(this.getId()));
        return loadVectorDuplicate;
    }

    private void duplicateElementsInListGivenTimes(List<String> splitComponent, int howManyNodes) {
        if (splitComponent.size() != howManyNodes) {
            for (int i = 0; i < howManyNodes; i++) {
                splitComponent.add(splitComponent.get(0));
            }
        }
    }

    @Override
    public void increaseMagnitude(double magnitudeToAdd) {
        magnitude += magnitudeToAdd;
    }

    @Override
    public void addVector(LoadVector loadVectorToAdd) {
        if (!loadVectorToAdd.getLoadType().equals(this.loadType)) {
            return;
        }
        if (!loadVectorToAdd.getNode().equals(this.node)) {
            return;
        }

        double x1 = x;
        double y1 = y;
        double z1 = z;

        double magnitude2 = Double.parseDouble(loadVectorToAdd.getMagnitude());

        double x2 = Double.parseDouble(loadVectorToAdd.getX());
        double y2 = Double.parseDouble(loadVectorToAdd.getY());
        double z2 = Double.parseDouble(loadVectorToAdd.getZ());

        double cosAlfaBetween1and2 = (x1 * x2 + y1 * y2 + z1 * z2) / (magnitude * magnitude2);

        this.magnitude = Math.sqrt(magnitude * magnitude + magnitude2 * magnitude2
                + 2 * magnitude * magnitude2 * cosAlfaBetween1and2);

        magnitude = Math.round(magnitude * 10.0) / 10.0;

        x = x1 + x2;
        x = Math.round(x * 10.0) / 10.0;
        y = y1 + y2;
        y = Math.round(y * 10.0) / 10.0;
        z = z1 + z2;
        z = Math.round(z * 10.0) / 10.0;

        cosAlfaX = x / magnitude;
        cosAlfaX = Math.round(cosAlfaX * 10000.0) / 10000.0;
        cosAlfaY = y / magnitude;
        cosAlfaY = Math.round(cosAlfaY * 10000.0) / 10000.0;
        cosAlfaZ = z / magnitude;
        cosAlfaZ = Math.round(cosAlfaZ * 10000.0) / 10000.0;

        StatusCMDPrinter.print("     + total vector calculated.");
        Settings.status.appendNewLine("     + total vector calculated.");
    }

    //cid, node, angle, type
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MomentVector that = (MomentVector) o;

        if (Double.compare(that.cosAlfaX, cosAlfaX) != 0) return false;
        if (Double.compare(that.cosAlfaY, cosAlfaY) != 0) return false;
        if (Double.compare(that.cosAlfaZ, cosAlfaZ) != 0) return false;
        if (loadType != that.loadType) return false;
        if (cid != null ? !cid.equals(that.cid) : that.cid != null) return false;
        return node != null ? node.equals(that.node) : that.node == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = loadType != null ? loadType.hashCode() : 0;
        result = 31 * result + (cid != null ? cid.hashCode() : 0);
        result = 31 * result + (node != null ? node.hashCode() : 0);
        temp = Double.doubleToLongBits(cosAlfaX);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(cosAlfaY);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(cosAlfaZ);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return String.format("%s: " +
                "N= %s, cosX=%2.2f, cosY=%2.2f, cosZ=%2.2f, " +
                "MAG= %2.0f", loadType.toString(), node, cosAlfaX, cosAlfaY, cosAlfaZ, magnitude);
    }
}
