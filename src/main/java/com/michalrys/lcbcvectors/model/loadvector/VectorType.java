package com.michalrys.lcbcvectors.model.loadvector;

public enum VectorType {
    FORCE,
    MOMENT,
    PLOAD4,
    GRAV;
}
