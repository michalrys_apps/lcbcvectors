package com.michalrys.lcbcvectors.model.settings;

import com.michalrys.lcbcvectors.model.excel.SumAlgorithm;
import com.michalrys.lcbcvectors.model.status.StatusObservableImpl;
import com.michalrys.lcbcvectors.model.viewoptions.ViewOptions;
import com.michalrys.lcbcvectors.tools.AppInfo;

public class Settings {
    public static StatusObservableImpl status;
    public static ViewOptions viewOptions = new ViewOptions();
    public static SumAlgorithm sumAlgorithm = SumAlgorithm.setDefault();

    public static void run() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                status = new StatusObservableImpl();
                status.set(AppInfo.INTRO);
                status.setRunning(false);
            }
        }).start();
    }
}