package com.michalrys.lcbcvectors.model.status;

public interface Status {
    interface Observer {
        void update(String status);
    }

    interface Observable {
        void addObserver(Status.Observer observer);

        void removeObserver(Status.Observer observer);

        void notifyObservers(int delayInMiliseconds);
    }
}