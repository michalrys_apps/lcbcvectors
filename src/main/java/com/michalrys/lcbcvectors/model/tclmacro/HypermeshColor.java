package com.michalrys.lcbcvectors.model.tclmacro;

public enum HypermeshColor {
    _BLACK(1, "black"),
    _GRAV(6, "yellow"),
    _FORCE(4, "green"),
    _MOMENT(7, "aqua"),
    _PLOAD4(8, "magenta"),
    _SPC(1, "black"),
    _SPC_X(3, "red"),
    _SPC_Y(4, "green"),
    _SPC_Z(5, "blue"),
    _WRONG(3, "red");

    private int hypermeshColorId;
    private String description;

    HypermeshColor(int hypermeshColorId, String description) {
        this.hypermeshColorId = hypermeshColorId;
        this.description = description;
    }

    public int getHypermeshColorId() {
        return hypermeshColorId;
    }

    public String getDescription() {
        return description;
    }
}
