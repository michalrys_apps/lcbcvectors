package com.michalrys.lcbcvectors.model.tclmacro;

import com.michalrys.lcbcvectors.model.lccollector.LCCollector;

import java.util.Map;

public interface TCLMacro {
    void setLCCollectors(Map<String, LCCollector> lcCollectors);

    String prepareMacro();
}
