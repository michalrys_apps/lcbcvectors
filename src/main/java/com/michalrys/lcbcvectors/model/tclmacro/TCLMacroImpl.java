package com.michalrys.lcbcvectors.model.tclmacro;

import com.michalrys.lcbcvectors.model.lccollector.LCCollector;
import com.michalrys.lcbcvectors.model.loadvector.LoadVector;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TCLMacroImpl implements TCLMacro {
    private Map<String, LCCollector> lcCollectors;

    @Override
    public void setLCCollectors(Map<String, LCCollector> lcCollectors) {
        this.lcCollectors = lcCollectors;
    }

    @Override
    public String prepareMacro() {
        StringBuffer tclMacro = new StringBuffer();
        tclMacro.append("# LCBC Vectors -> author: M. Ryś (michalrys@gmail.com)\n");
        tclMacro.append("# Freeware for EC-E employees\n");
        tclMacro.append("# Created in: "
                + LocalDateTime.now().format(DateTimeFormatter.ofPattern("YY-MM-dd_hh-mm-ss")) + "\n");
        tclMacro.append("# ---------------------------------------\n\n");
        tclMacro.append("# create dummy vector\n");
        tclMacro.append("*createnode 0 0 0 0\n");
        tclMacro.append("*createmark nodes 1 -1\n");
        tclMacro.append("*vectorcreate 1 -5 -5 0 0\n\n");

        tclMacro.append("# remove all vectors\n");
        tclMacro.append("*createmark vectors 1 \"all\"\n");
        tclMacro.append("*deletemark vectors 1\n\n");

        tclMacro.append("# remove all vector collectors\n");
        tclMacro.append("*createmark vectorcols 1 \"all\"\n");
        tclMacro.append("*deletemark vectorcols 1\n\n");

        tclMacro.append("# set vector draw option\n");
        tclMacro.append("*vectorlabel 1  # label on\n");
        tclMacro.append("*vectordrawoptions 200 0 5 # by uniform size\n");
        tclMacro.append("#  *vectordrawoptions 200 1 1000 # by magnitude in %\n\n");

        tclMacro.append("# set node for gravities\n");
        tclMacro.append("*createnode 0 0 0\n");
        tclMacro.append("*createmark nodes 1 -1\n");
        tclMacro.append("set nodeIdForGravities [hm_getmark nodes 1]\n\n");
        tclMacro.append("# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - \n\n");

        tclMacro.append("# list of colors\n");
        for (HypermeshColor hypermeshColor : HypermeshColor.values()) {
            tclMacro.append("# for vector collector ..." + hypermeshColor.toString());
            tclMacro.append(" was used " + hypermeshColor.getDescription());
            tclMacro.append(" color, with hm color id " + hypermeshColor.getHypermeshColorId() + "\n");
        }
        tclMacro.append("# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - \n\n");

        Set<String> keys = lcCollectors.keySet();
        for (String hyperMeshVectorCollector : keys) {
            tclMacro.append("# create vector collector, name, empty, color id\n");
            int color = calculateColor(hyperMeshVectorCollector);
            tclMacro.append("*collectorcreateonly vectorcols \"" + hyperMeshVectorCollector + "\" \"\" "
                    + color + "\n\n");

            LCCollector lcCollector = lcCollectors.get(hyperMeshVectorCollector);
            List<LoadVector> vectors = lcCollector.getLoadVectors();

            if (Pattern.compile(".*_PLOAD4$").matcher(hyperMeshVectorCollector).matches()) {
                for (LoadVector vector : vectors) {
                    if (vector.getNode().equals("CHARACTERS LIMIT REACHED")) {
                        tclMacro.append("# !!! pload reached the characters limit - to many elements for pload4. This " +
                                "pressure has been omitted.\n");
                        tclMacro.append("*collectorcreateonly vectorcols \"" + hyperMeshVectorCollector + "!_problem_with_" +
                                "some_pressure\" \"\" "
                                + color + "\n\n");
                        continue;
                    }
                    tclMacro.append("# create mid node for element id\n");
                    String elementId = vector.getNode();

                    tclMacro.append("set quadElemetEvaluation true\n");
                    tclMacro.append("if { [string match [hm_getentityvalue element " + elementId + " node4.id 0] 0]} {\n");
                    tclMacro.append("   set quadElemetEvaluation false\n");
                    tclMacro.append("}\n\n");
                    tclMacro.append("if {$quadElemetEvaluation} {\n");
                    tclMacro.append("    *nodecleartempmark\n");
                    tclMacro.append("    *createnodesbetweennodes " +
                            "[hm_getentityvalue element " + elementId + " node1.id 0] " +
                            "[hm_getentityvalue element " + elementId + " node2.id 0] 1\n");
                    tclMacro.append("    *createnodesbetweennodes " +
                            "[hm_getentityvalue element " + elementId + " node3.id 0] " +
                            "[hm_getentityvalue element " + elementId + " node4.id 0] 1\n");
                    tclMacro.append("    *createnodesbetweennodes -1 -2 1\n");
                    tclMacro.append("} else {\n");
                    tclMacro.append("    *nodecleartempmark\n");
                    tclMacro.append("    *createnodesbetweennodes " +
                            "[hm_getentityvalue element " + elementId + " node1.id 0] " +
                            "[hm_getentityvalue element " + elementId + " node2.id 0] 1\n");
                    tclMacro.append("    *createnodesbetweennodes -1 " +
                            "[hm_getentityvalue element " + elementId + " node3.id 0] 1\n");
                    tclMacro.append("}\n\n");


                    tclMacro.append("# mark node\n");
                    tclMacro.append("*createmark nodes 1 -1\n\n");

                    String x = vector.getX();
                    String y = vector.getY();
                    String z = vector.getZ();
                    String cid = vector.getCID();

                    tclMacro.append("# create vector with x,y,z,coord id\n");
                    tclMacro.append("*vectorcreate 1 " + x + " " + y + " " + z + " " + cid + "\n");
                    tclMacro.append("# mark created vector\n");
                    tclMacro.append("*createmark vectors 1 -1\n");
                    tclMacro.append("# set magnitude\n");
                    tclMacro.append("*vectorupdate 1 0 0 0 " + vector.getMagnitude() + " 0 1 0 0\n\n");

                    tclMacro.append("#clear temp nodes\n");
                    tclMacro.append("*nodecleartempmark\n\n");
                }

            } else if (Pattern.compile(".*_GRAV$").matcher(hyperMeshVectorCollector).matches()) {
                for (LoadVector vector : vectors) {
                    tclMacro.append("# mark node for gravity load\n");
                    tclMacro.append("*createmark nodes 1 $nodeIdForGravities\n\n");

                    String x = vector.getX();
                    String y = vector.getY();
                    String z = vector.getZ();
                    String cid = vector.getCID();

                    tclMacro.append("# create vector with x,y,z,coord id\n");
                    tclMacro.append("*vectorcreate 1 " + x + " " + y + " " + z + " " + cid + "\n");
                    tclMacro.append("# mark created vector\n");
                    tclMacro.append("*createmark vectors 1 -1\n");
                    tclMacro.append("# set magnitude\n");
                    tclMacro.append("*vectorupdate 1 0 0 0 " + vector.getMagnitude() + " 0 1 0 0\n");

                    tclMacro.append("\n\n");
                }
            } else {
                for (LoadVector vector : vectors) {
                    tclMacro.append("# mark node\n");
                    tclMacro.append("*createmark nodes 1 " + vector.getNode() + "\n\n");

                    String x = vector.getX();
                    String y = vector.getY();
                    String z = vector.getZ();
                    String cid = vector.getCID();

                    tclMacro.append("# create vector with x,y,z,coord id\n");
                    tclMacro.append("*vectorcreate 1 " + x + " " + y + " " + z + " " + cid + "\n");
                    tclMacro.append("# mark created vector\n");
                    tclMacro.append("*createmark vectors 1 -1\n");
                    tclMacro.append("# set magnitude\n");
                    tclMacro.append("*vectorupdate 1 0 0 0 " + vector.getMagnitude() + " 0 1 0 0\n");

                    tclMacro.append("\n\n");
                }
            }


        }
        return tclMacro.toString();
    }

    private int calculateColor(String hyperMeshVectorCollector) {
        Pattern patternForce = Pattern.compile(".*" + HypermeshColor._FORCE.toString() + "$");
        Matcher matcherForce = patternForce.matcher(hyperMeshVectorCollector);
        Pattern patternMoment = Pattern.compile(".*" + HypermeshColor._MOMENT.toString() + "$");
        Matcher matcherMoment = patternMoment.matcher(hyperMeshVectorCollector);
        Pattern patternPressure = Pattern.compile(".*" + HypermeshColor._PLOAD4.toString() + "$");
        Matcher matcherPressure = patternPressure.matcher(hyperMeshVectorCollector);
        Pattern patternGravity = Pattern.compile(".*" + HypermeshColor._GRAV.toString() + "$");
        Matcher matcherGravity = patternGravity.matcher(hyperMeshVectorCollector);

        if (matcherForce.matches()) {
            return HypermeshColor._FORCE.getHypermeshColorId();
        } else if (matcherMoment.matches()) {
            return HypermeshColor._MOMENT.getHypermeshColorId();
        } else if (matcherPressure.matches()) {
            return HypermeshColor._PLOAD4.getHypermeshColorId();
        } else if(matcherGravity.matches()) {
            return HypermeshColor._GRAV.getHypermeshColorId();
        }
        return HypermeshColor._BLACK.getHypermeshColorId();
    }
}