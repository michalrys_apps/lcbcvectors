package com.michalrys.lcbcvectors.model.viewoptions;

import com.michalrys.lcbcvectors.model.loadvector.VectorType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum ViewOption {
    EXTRACT_GRAV(true),
    EXTRACT_FORCES(true),
    EXTRACT_MOMENTS(true),
    EXTRACT_PRESSURES(true),
    EXTRACT_BCS(false),
    VECTORS_PER_LC(true),
    VECTORS_PER_LC_AND_PER_LOADNAME(false),
    SUM_BY_LOCATION_AND_BY_DIRECTION(true),
    SUM_BY_LOCATION(false);

    private boolean isTurnedOn;

    ViewOption(boolean isTurnedOn) {
        this.isTurnedOn = isTurnedOn;
    }

    public boolean isTurnedOn() {
        return isTurnedOn;
    }

    public void turnOn() {
        isTurnedOn = true;
    }

    public void turnOf() {
        isTurnedOn = false;
    }

    public static Map<ViewOption, Boolean> getDefault() {
        ViewOption[] values = ViewOption.values();
        Map<ViewOption, Boolean> result = new HashMap<>();
        for (ViewOption viewOption : values) {
            result.put(viewOption, viewOption.isTurnedOn);
        }
        return result;
    }

    public static List<VectorType> getListOfVectorTypeToExtract(Map<ViewOption, Boolean> viewOptions) {
        ArrayList<VectorType> vectorTypesToExtract = new ArrayList<>();

        for (ViewOption viewOptionKey : viewOptions.keySet()) {
            if (viewOptionKey.equals(ViewOption.SUM_BY_LOCATION) ||
                    viewOptionKey.equals(ViewOption.SUM_BY_LOCATION_AND_BY_DIRECTION) ||
                    viewOptionKey.equals(ViewOption.VECTORS_PER_LC) ||
                    viewOptionKey.equals(ViewOption.VECTORS_PER_LC_AND_PER_LOADNAME)) {
                continue;
            }
            Boolean shouldBeExtracted = viewOptions.get(viewOptionKey);
            if(!shouldBeExtracted) {
                continue;
            }

            switch (viewOptionKey) {
                case EXTRACT_GRAV:
                    vectorTypesToExtract.add(VectorType.GRAV);
                    break;
                case EXTRACT_FORCES:
                    vectorTypesToExtract.add(VectorType.FORCE);
                    break;
                case EXTRACT_PRESSURES:
                    vectorTypesToExtract.add(VectorType.PLOAD4);
                    break;
                case EXTRACT_MOMENTS:
                    vectorTypesToExtract.add(VectorType.MOMENT);
                    break;
                case EXTRACT_BCS:
                    break;
            }
        }
        return vectorTypesToExtract;
    }
}