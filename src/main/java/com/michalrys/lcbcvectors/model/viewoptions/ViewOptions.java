package com.michalrys.lcbcvectors.model.viewoptions;

import com.michalrys.lcbcvectors.model.loadvector.VectorType;

import java.util.List;
import java.util.Map;

public class ViewOptions {
    private static Map<ViewOption, Boolean> viewOptions = ViewOption.getDefault();

    public void set(ViewOption viewOption, boolean value) {
        viewOptions.put(viewOption, value);
    }

    public boolean get(ViewOption viewOption) {
        return viewOptions.get(viewOption);
    }

    public List<VectorType> getVectorTypes() {
        return ViewOption.getListOfVectorTypeToExtract(viewOptions);
    }
}