package com.michalrys.lcbcvectors.tools;

public class AppInfo {
    public static final String NAME = "LC/BC Vectors <--";
    public static final String AUTHOR = "Michał Ryś";
    public static final String VERSION = "1.0.4.beta";
    public static final String DATE = "2019-11-13";
    public static final String LICENSE = "Freeware for EC-E employees";

    public static final String INTRO = NAME + " version " + VERSION + " (" + DATE + ").";
    public static final String AUTHOR_LICENSE_VERSION = "Author: " + AUTHOR + ", " + LICENSE + ".\n"
            + "Version: " + VERSION + " (" + DATE + ").";
}
