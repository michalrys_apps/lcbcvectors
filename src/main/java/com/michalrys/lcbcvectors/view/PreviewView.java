package com.michalrys.lcbcvectors.view;

import com.michalrys.lcbcvectors.MVC;
import com.michalrys.lcbcvectors.RunAppJavaFX;
import com.michalrys.lcbcvectors.controller.LCBCVectorsController;
import com.michalrys.lcbcvectors.model.excel.SumAlgorithm;
import com.michalrys.lcbcvectors.model.settings.Settings;
import com.michalrys.lcbcvectors.model.status.Status;
import com.michalrys.lcbcvectors.model.viewoptions.ViewOption;
import com.michalrys.lcbcvectors.tools.AppInfo;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.input.*;

import java.io.File;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;

public class PreviewView implements Initializable, MVC.View, Status.Observer {
    private MVC.Controller controller;

    @FXML
    public CheckBox extractGravs;

    @FXML
    public CheckBox extractFroces;

    @FXML
    public CheckBox extractMoments;

    @FXML
    public CheckBox extractPressures;

    @FXML
    public CheckBox extractBCs;

    @FXML
    public Label versionAuthor;

    @FXML
    public RadioButton vectorsPerLC;

    @FXML
    public RadioButton vectorsPerLCandLoadName;

    @FXML
    public RadioButton sumByLocationAndByDirection;

    @FXML
    public RadioButton sumByLocation;

    @FXML
    public TextArea textAreaPreview;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        controller = new LCBCVectorsController(this);
        Settings.status.addObserver(this);

        if (LocalDate.now().isAfter(LocalDate.parse("2019-12-01"))) {
            Settings.status.set("This was only a beta version. It is not up to date now. Nothing can be done.");
            Settings.status.appendNewLine("Ask MR:   michalrys@gmail.com");

            textAreaPreview.setDisable(true);
            extractGravs.setDisable(true);
            extractFroces.setDisable(true);
            extractMoments.setDisable(true);
            extractPressures.setDisable(true);
            vectorsPerLC.setDisable(true);
            vectorsPerLCandLoadName.setDisable(true);
            sumByLocation.setDisable(true);
            sumByLocationAndByDirection.setDisable(true);
        } else {
            runWithInputArgs();
        }
    }

    public void statusDragOver(DragEvent dragEvent) {
        dragEvent.acceptTransferModes(TransferMode.COPY_OR_MOVE);
        dragEvent.consume();
    }

    public void statusOnDragDropped(DragEvent dragEvent) {
        Dragboard dragboard = dragEvent.getDragboard();
        List<File> files = dragboard.getFiles();
        dragEvent.setDropCompleted(true);
        dragEvent.consume();

        File lastFile = files.get(files.size() - 1);
        String[] inputArg = new String[1];
        inputArg[0] = lastFile.getAbsolutePath();

        readCurrentOptions();
        controller.runWithDragAndDropFile(inputArg);
    }

    private void readCurrentOptions() {
        Settings.viewOptions.set(ViewOption.EXTRACT_GRAV, extractGravs.isSelected());
        Settings.viewOptions.set(ViewOption.EXTRACT_FORCES, extractFroces.isSelected());
        Settings.viewOptions.set(ViewOption.EXTRACT_MOMENTS, extractMoments.isSelected());
        Settings.viewOptions.set(ViewOption.EXTRACT_PRESSURES, extractPressures.isSelected());
        Settings.viewOptions.set(ViewOption.EXTRACT_BCS, extractBCs.isSelected());
        Settings.viewOptions.set(ViewOption.VECTORS_PER_LC, vectorsPerLC.isSelected());
        Settings.viewOptions.set(ViewOption.VECTORS_PER_LC_AND_PER_LOADNAME, vectorsPerLCandLoadName.isSelected());
        Settings.viewOptions.set(ViewOption.SUM_BY_LOCATION_AND_BY_DIRECTION, sumByLocationAndByDirection.isSelected());
        Settings.viewOptions.set(ViewOption.SUM_BY_LOCATION, sumByLocation.isSelected());
    }

    public void versionAuthorClicked(MouseEvent mouseEvent) {
        MouseButton button = mouseEvent.getButton();
        if (button.equals(MouseButton.SECONDARY)) {
            Settings.status.clear();
        } else {
            Settings.status.appendNewLine(AppInfo.AUTHOR_LICENSE_VERSION);
        }
    }


    public void checkboxGravsClicked(MouseEvent mouseEvent) {
        if (extractGravs.isSelected()) {
            Settings.status.appendNewLine("Gravs on.");
        } else {
            Settings.status.appendNewLine("Gravs off.");
        }
    }

    public void checkboxForcesClicked(MouseEvent mouseEvent) {
        if (extractFroces.isSelected()) {
            Settings.status.appendNewLine("Forces on.");
        } else {
            Settings.status.appendNewLine("Forces off.");
        }
    }

    public void checkboxMomentsClicked(MouseEvent mouseEvent) {
        if (extractMoments.isSelected()) {
            Settings.status.appendNewLine("Moments on.");
        } else {
            Settings.status.appendNewLine("Moments off.");
        }
    }

    public void checkboxPressuresClicked(MouseEvent mouseEvent) {
        if (extractPressures.isSelected()) {
            Settings.status.appendNewLine("Pressures on.");
        } else {
            Settings.status.appendNewLine("Pressures off.");
        }
    }

    public void checkboxBCsClicked(MouseEvent mouseEvent) {
        if (extractBCs.isSelected()) {
            Settings.status.appendNewLine("BCs on.");
        } else {
            Settings.status.appendNewLine("BCs off.");
        }
    }

    public void radioButtonVectorsPerLCClicked(MouseEvent mouseEvent) {
        if (vectorsPerLC.isSelected()) {
            vectorsPerLCandLoadName.setSelected(false);
            Settings.status.appendNewLine("Vectors per LC on.");
        } else {
            vectorsPerLCandLoadName.setSelected(true);
            Settings.status.appendNewLine("Vectors per LC and per load name on.");
        }
    }

    public void radioButtonVectorsPerLCandPerLoadNameClicked(MouseEvent mouseEvent) {
        if (vectorsPerLCandLoadName.isSelected()) {
            vectorsPerLC.setSelected(false);
            Settings.status.appendNewLine("Vectors per LC and per load name on.");
        } else {
            vectorsPerLC.setSelected(true);
            Settings.status.appendNewLine("Vectors per LC on.");
        }
    }

    public void radioButtonSumByLocationAndByDirectionClicked(MouseEvent mouseEvent) {
        if (sumByLocationAndByDirection.isSelected()) {
            sumByLocation.setSelected(false);
            Settings.sumAlgorithm = SumAlgorithm.BY_LOCATION_AND_BY_DIRECTION;
            Settings.status.appendNewLine("Sum by location and by direction on.");
        } else {
            sumByLocation.setSelected(true);
            Settings.sumAlgorithm = SumAlgorithm.BY_LOCATION;
            Settings.status.appendNewLine("Sum by location on.");
        }
    }

    public void radioButtonSumByLocationClicked(MouseEvent mouseEvent) {
        if (sumByLocation.isSelected()) {
            sumByLocationAndByDirection.setSelected(false);
            Settings.sumAlgorithm = SumAlgorithm.BY_LOCATION;
            Settings.status.appendNewLine("Sum by location on.");
        } else {
            sumByLocationAndByDirection.setSelected(true);
            Settings.sumAlgorithm = SumAlgorithm.BY_LOCATION_AND_BY_DIRECTION;
            Settings.status.appendNewLine("Sum by location and by direction on.");
        }
    }

    @Override
    public void runWithInputArgs() {
        controller.runWithInputArgs(RunAppJavaFX.inputArgs);
    }

    @Override
    public void update(String status) {
        textAreaPreview.setText(status);
        textAreaPreview.selectEnd();
        textAreaPreview.deselect();
    }
}