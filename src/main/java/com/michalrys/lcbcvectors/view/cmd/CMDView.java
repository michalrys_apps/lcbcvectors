package com.michalrys.lcbcvectors.view.cmd;

import com.michalrys.lcbcvectors.model.lcbcvectors.LCBCVectors;
import com.michalrys.lcbcvectors.model.lcbcvectors.LCBCVectorsImpl;

import java.io.File;

public class CMDView {
    public static void main(String[] args) {
        System.out.println("LC BC Vectors (version 1.0.0.beta : M.RYŚ 2019-10-28)\n");
        checkArgs(args);

        File file = extractFirstFile(args);
        showMessageFile(file);

        LCBCVectors lcbcVectors = new LCBCVectorsImpl();

        lcbcVectors.setExcelFile(file);
        lcbcVectors.setUp();
        lcbcVectors.extractData();
        lcbcVectors.writeToTCLFile();
        lcbcVectors.getTclFilePath();
    }

    private static void checkArgs(String[] args) {
        if (args.length == 0) {
            System.out.println("PROBLEM: no file was selected. Nothing was done. Exited.");
            //System.exit(0);
        }
    }

    private static File extractFirstFile(String[] args) {
        String arg = args[0];
        File file = new File(arg);
        exitWhenFileDoesNotExist(file);
        return file;
    }

    private static void exitWhenFileDoesNotExist(File file) {
        if (!file.exists()) {
            System.out.println("PROBLEM: wrong given file: " + file.getAbsolutePath() + ". Nothing was done. Exited.");
            //System.exit(0);
        }
    }

    private static void showMessageFile(File file) {
        System.out.println("Given file: " + file.getName());
    }
}
