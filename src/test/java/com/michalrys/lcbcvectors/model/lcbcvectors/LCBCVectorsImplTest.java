package com.michalrys.lcbcvectors.model.lcbcvectors;

import com.michalrys.lcbcvectors.model.settings.Settings;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LCBCVectorsImplTest {

    @Before
    public void setUp() {
        Settings.run();
    }


    @Test
    public void shouldSetExcelFileIfFileIsLCBCMacroExtracted() {
        //given
        LCBCVectors lcbcVectors = new LCBCVectorsImpl();
        File file = new File("./src/main/resources/TEST_FILE_01.xlsx");

        //when
        boolean isExcelFileSet = lcbcVectors.setExcelFile(file);

        //then
        Assert.assertTrue(file.exists());
        Assert.assertTrue(isExcelFileSet);
    }

    @Test
    public void shouldGetTclFilePath() {
        //given
        LCBCVectors lcbcVectors = new LCBCVectorsImpl();
        File file = new File("./src/main/resources/TEST_FILE_01.xlsx");
        String tclFilePathExpected = new File("./src/main/resources/TEST_FILE_01_LCBCVECTORS_macro_"
                + LocalDateTime.now().format(DateTimeFormatter.ofPattern("YYMMdd-hhmmss")) + ".tcl").getAbsolutePath();
        lcbcVectors.setExcelFile(file);

        //when
        String tclFilePath = lcbcVectors.getTclFilePath();

        //then
        Assert.assertEquals(tclFilePathExpected, tclFilePath);
    }

    @Test
    public void shouldWriteTCLMacroFile() {
        //given
        LCBCVectors lcbcVectors = new LCBCVectorsImpl();
        File file = new File("./src/main/resources/TEST_FILE_01.xlsx");
        lcbcVectors.setExcelFile(file);
        lcbcVectors.setUp();
        lcbcVectors.extractData();

        //when
        lcbcVectors.writeToTCLFile();

        //then
        Assert.assertTrue(new File(lcbcVectors.getTclFilePath()).exists());
        new File(lcbcVectors.getTclFilePath()).delete();
    }

    @Test
    public void shouldWriteTCLMacroFileWithCorrectContent() {
        //given
        LCBCVectors lcbcVectors = new LCBCVectorsImpl();
        File file = new File("./src/main/resources/TEST_FILE_01.xlsx");
        String macroExpectedData = "test 2";

        lcbcVectors.setExcelFile(file);
        lcbcVectors.setUp();
        lcbcVectors.extractData();

        //when
        lcbcVectors.writeToTCLFile();

        File fileResult = new File(lcbcVectors.getTclFilePath());
        String results = readFile(fileResult);

        //then
        //new File(lcbcVectors.getTclFilePath()).delete();
        Assert.assertEquals(macroExpectedData, results);

    }

    @Test
    public void shouldWriteTCLMacroFileWithCorrectContent2() {
        //given
        LCBCVectors lcbcVectors = new LCBCVectorsImpl();
        File file = new File("./src/main/resources/TEST2.xlsx");
        String macroExpectedData = "test 2";

        lcbcVectors.setExcelFile(file);
        lcbcVectors.setUp();
        lcbcVectors.extractData();

        //when
        lcbcVectors.writeToTCLFile();

        File fileResult = new File(lcbcVectors.getTclFilePath());
        String results = readFile(fileResult);

        //then
        new File(lcbcVectors.getTclFilePath()).delete();
        Assert.assertEquals(macroExpectedData, results);
    }

    @Test
    public void shouldWriteTCLMacroFileWithCorrectContent3() {
        //given
        LCBCVectors lcbcVectors = new LCBCVectorsImpl();
        File file = new File("./src/main/resources/TEST_FILE_02.xlsx");
        String macroExpectedData = "test 2";

        lcbcVectors.setExcelFile(file);
        lcbcVectors.setUp();
        lcbcVectors.extractData();

        //when
        lcbcVectors.writeToTCLFile();

        File fileResult = new File(lcbcVectors.getTclFilePath());
        String results = readFile(fileResult);

        //then
        //new File(lcbcVectors.getTclFilePath()).delete();
        Assert.assertEquals(macroExpectedData, results);
    }

    @Test
    public void shouldWriteTCLMacroFileWithCorrectContent3WithPressureCharactersLimitReached() {
        //given
        LCBCVectors lcbcVectors = new LCBCVectorsImpl();
        File file = new File("./src/main/resources/TEST_FILE_03_characters_limit.xlsx");
        String macroExpectedData = "test 3 pressure limit reached";

        lcbcVectors.setExcelFile(file);
        lcbcVectors.setUp();
        lcbcVectors.extractData();

        //when
        lcbcVectors.writeToTCLFile();

        File fileResult = new File(lcbcVectors.getTclFilePath());
        String results = readFile(fileResult);

        //then
        //new File(lcbcVectors.getTclFilePath()).delete();
        Assert.assertEquals(macroExpectedData, results);
    }

    @Test
    public void shouldWriteTCLMacroFileWithCorrectContent4GravTest() {
        //given
        LCBCVectors lcbcVectors = new LCBCVectorsImpl();
        File file = new File("./src/main/resources/TEST_FILE_04_grav_test.xlsx");
        String macroExpectedData = "test 4 grav test";

        lcbcVectors.setExcelFile(file);
        lcbcVectors.setUp();
        lcbcVectors.extractData();

        //when
        lcbcVectors.writeToTCLFile();

        File fileResult = new File(lcbcVectors.getTclFilePath());
        String results = readFile(fileResult);

        //status of application:
        System.out.println(Settings.status.get());
        //then
        //new File(lcbcVectors.getTclFilePath()).delete();
        Assert.assertEquals(macroExpectedData, results);
    }

    private String readFile(File fileResult) {
        StringBuffer results = new StringBuffer();
        String line;
        try (BufferedReader reader = new BufferedReader(new FileReader(fileResult))) {
            while ((line = reader.readLine()) != null) {
                results.append(line + "\n");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        results.replace(results.length() - 1, results.length(), "");
        return results.toString();
    }

}