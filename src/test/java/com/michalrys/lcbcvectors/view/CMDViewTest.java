package com.michalrys.lcbcvectors.view;

import com.michalrys.lcbcvectors.view.cmd.CMDView;
import org.junit.Test;

public class CMDViewTest {

    @Test
    public void shouldExitedWhenNoInputArgs() {
        //given
        CMDView.main(new String[0]);
        //when
        //then
    }

    @Test
    public void shouldExitedWhenGivenArgIsNotAFile() {
        //given
        String [] arg = {"not a file"};
        CMDView.main(arg);
        //when
        //then
    }

    @Test
    public void shouldRunExtractorButFailDueToWrongKindOfFile() {
        //given
        String [] arg = {"c:/Windows/explorer.exe"};
        CMDView.main(arg);
        //when
        //then
    }

}